#!/usr/bin/env python3
import requests, json, getpass, sys
from requests.auth import HTTPBasicAuth

def request():
    response = requests.get(url, verify=False, auth=auth, headers=headers)
    statusCode = str(response.status_code)
    if '400' in statusCode:
        print('400, Bad Request. Make sure you passed the right URL')
        sys.exit(1)
    elif '401' in statusCode:
        print('401, Authentication Error. Please verify Username and/or Password')
        sys.exit(1)
    elif '403' in statusCode:
        print('403, Access Forbidden. Make sure you have the right permission to access to the URL required')
        sys.exit(1)
    elif '404' in statusCode:
        print('404, Page Not Found. What you are looking for it doesn\'t exist')
        sys.exit(1)
    elif '200' in statusCode:
        respDict = json.loads(response.text)
        for values in respDict.values():
            for entries in values:
                print('{:20}'.format(entries['IPAddress']) + entries['Caption'])
    else:
        print('Something went wrong with the GET request/respons. Debug will bring light in your dark and broken code')
        sys.exit(1)

def main():
    request()

if __name__ == '__main__':
    '''Solarwind devices management IP scrub via API'''
    username = input('Username: ')
    password = getpass.getpass()
    auth = HTTPBasicAuth(username, password)
    url = sys.argv[1]
    headers = { 'Content-Type': 'application/json' }
    main()
